# MIDDLEWARE PERMISOS SOA
## Servicio de Salud Aysén

![N|Solid](https://login.saludaysen.cl/img/logo-ssa-white.png)

##Caracteristicas

- Permite o no el acceso a una ruta determinada, si es que se ha entregado el permiso al usuario que inicia sesión

## Instalacion

URL del paquete [package.org](https://packagist.org/packages/joost/middleware#v1.0.0)

Se debe instalar el paquete ingresando al contenedor del sistema.
```sh
composer require joost/middleware
```

Luego es necesario agrega la siguiente variable, indicando el endpoint de autenticación según el ambiente en el que este sistema

```sh
ENDPOINT_PERMISOS_MIDDLEWARE=(ej:http://192.168.1.1:2021)
```

## Configuración

Se debe agregar la siguiente linea en el archivo kernel al array $routeMiddleware
```sh
'ssa.permisos' => \Joost\Middleware\PermisosMiddleware::class,
```

Luego se debe crear un grupo para agregar todas las rutas que deban utilizar los permisos.
```sh
Route::middleware(["ssa.permisos"])->group(function () {
  ...
});
```
