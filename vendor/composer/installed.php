<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'cc6372e79c15e27744ebfa33bbeda371c02210e2',
    'name' => 'joost/middleware',
  ),
  'versions' => 
  array (
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.1.x-dev',
      'version' => '2.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '3249de584daab089c823ddc3f8efb108cee85a10',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => '8.x-dev',
      'version' => '8.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'fc232e89c0214dba5d2b431220a24b02d480a472',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => '8.x-dev',
      'version' => '8.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '5e0fd287a1b22a6b346a9f7cd484d8cf0234585d',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => '8.x-dev',
      'version' => '8.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'aed81891a6e046fdee72edd497f822190f61c162',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => '8.x-dev',
      'version' => '8.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '3f1de19528fc235d666f73d540d13a684da6bf3a',
    ),
    'joost/middleware' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'cc6372e79c15e27744ebfa33bbeda371c02210e2',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'cd60719e6217fad102a0b97286981c78212c4eb8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => '6.0.x-dev',
      'version' => '6.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '3d38cf8f8834148c4457681d539bc204de701501',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c4183fc3ef0f0510893cbeedc7718fb5cafc9ac9',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '87337c91b9dfacee02452244ee14ab3c43bc485a',
    ),
  ),
);
