<?php

namespace Joost\Middleware;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class PermisosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //VARIABLE PARA TRABAJAR CON FLUJO SOA COMO DESARROLLADOR O EN PRODUCCION
        if(env('APP_WITH_SOA')){
            try {
                if ($request->header('Passport')) {
                    $headers['Authorization'] = 'Bearer ' . $request->header('Passport');
                    $headers['Accept'] = 'application/json';
                    $headers['cache-control'] = 'no-cache';

                    try {
                        $client = new Client([
                                'base_uri' => env('ENDPOINT_PERMISOS_MIDDLEWARE'),
                                'verify' => false
                            ]
                        );
                        $response = $client->request('GET', '/api/autenticacion/usuario', [
                            'debug' => false,
                            'headers' => $headers
                        ]);
                        $usuario = json_decode($response->getBody(), true);
                        $token_file = Storage::disk('local')->get('tokens/' . $usuario['id'] . '/passport.json');
                        $token = json_decode($token_file);
                        if ($token->establecimientos_asociados) {
                            $nombre_url = Route::getFacadeRoot()->current()->uri();
                            foreach ($token->establecimientos_asociados as $establecimiento) {
                                if (isset($establecimiento->asignado) && $establecimiento->asignado) {
                                    foreach ($establecimiento->proyectos as $key => $proyecto) {
                                        if (isset($proyecto->asignado) && $proyecto->asignado && $proyecto->perfil && $proyecto->perfil->acciones) {
                                            foreach ($proyecto->perfil->acciones as $key => $accion) {
                                                if ($nombre_url . '/' == "api" . $accion->ruta || $nombre_url == 'api' . $accion->ruta) {
                                                    return $next($request);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (ClientException $e) {
                        if ($e->getCode() == 401) {
                            return response()->json([
                                'estado' => false,
                                'mensaje' => 'No autorizado'
                            ], 401);
                        } else {
                            throw new HttpException(500, $e->getMessage());
                        }
                    }


                } else {
                    return response()->json([
                        'estado' => false,
                        'mensaje' => 'No autorizado'
                    ], 401);
                }

            } catch (\Throwable $t) {
                throw new HttpException(500, $t->getMessage());
            }

            return response()->json([
                'estado' => false,
                'mensaje' => 'No autorizado'
            ], 401);
        }
        else{
            return $next($request);
        }
    }
}
